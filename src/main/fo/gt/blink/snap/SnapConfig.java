package fo.gt.blink.snap;

import fo.gt.blink.matching.MatchingRule;

public class SnapConfig {
    private int numberOfDecks;
    private MatchingRule matchingRule;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SnapConfig that = (SnapConfig) o;

        if (numberOfDecks != that.numberOfDecks) return false;
        return ((matchingRule == null && that.matchingRule == null) || matchingRule.equals(that.matchingRule));
    }

    @Override
    public int hashCode() {
        int result = numberOfDecks;
        result = 31 * result + (matchingRule != null ? matchingRule.hashCode() : 0);
        return result;
    }

    public int getNumberOfDecks() {
        return numberOfDecks;
    }

    public MatchingRule getMatchingRule() {
        return matchingRule;
    }

    public int getNumberOfPlayers() {
        return 2;
    }

    public SnapConfig(int numberOfDecks, MatchingRule matchingRule) {
        this.numberOfDecks = numberOfDecks;
        this.matchingRule = matchingRule;
    }
}
