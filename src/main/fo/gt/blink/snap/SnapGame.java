package fo.gt.blink.snap;

import fo.gt.blink.cards.Card;
import fo.gt.blink.cards.PlayingDeck;
import fo.gt.blink.utils.ArrayOps;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SnapGame {
    private SnapConfig config;

    private Player[] players;
    private Iterator<Card> cardIt;
    private List<Card> currentRoundCards;
    private Player currentWinner;
    private PrintStream out;

    public SnapGame(SnapConfig config, PrintStream out) {
        this.config = config;
        this.out = out;
        this.currentWinner = null;

        // Set up the deck
        PlayingDeck deck = new PlayingDeck(config.getNumberOfDecks());
        deck.shuffle();
        this.cardIt = deck.iterator();
        this.currentRoundCards = new LinkedList<>();
        deal();

        // Set up the players
        int numPlayers = config.getNumberOfPlayers();
        this.players = new Player[numPlayers];
        for (int i = 0; i < numPlayers; ++i) {
            Player p = new Player(i);
            players[i] = p;
        }
    }

    /*
     * Step the simulation.
     * returns true if the game is over (if one or more players have no cards left)
     */
    public boolean step() {
        if (!cardIt.hasNext()) return true;

        // Deal another card
        deal();

        // Figure out if there is a match
        if (currentRoundCards.size() >= 2 && config.getMatchingRule().matches(currentRoundCards.get(0), currentRoundCards.get(1))) {
            // Pick the shouting order. NB there is a more "fun" approach here -- to have a bunch of threads compete for
            // some synchronized state and only allow the first one to cali to be a winner. The problem is that there
            // isn't a great way to allow a "random" thread to proceed from a synchronization barrier without invoking
            // some kind of random number generator to do something like introducing a random delay (threads are released
            // based on the order in which they arrive by at least CyclicBarrier and CountDownLatch IIRC). So if we're
            // going to have to use a random number generator anyway -- and we are only simulating the players having
            // a barney -- we might as well just randomize the order in which they shout when there's a match and leave
            // it at that.
            Player[] shouters = new Player[players.length];
            ArrayOps.randomPermutationCopy(players, shouters);
            for (int i = 0; i < shouters.length; ++i) {
                Player shouter = shouters[i];
                out.println(shouter.getName() + ": SNAP!");
            }
            Player winner = shouters[0];
            int score = currentRoundCards.size();
            winner.won(score);
            out.println(winner.getName() + " wins (+" + score + " -> " + winner.getScore() + ")");
            currentRoundCards.clear();
        }

        return false;
    }

    public Player[] getPlayers() {
        return players;
    }

    private void deal() {
        Card currentCard = cardIt.next();
        out.println("Dealt a " + currentCard.toString());
        currentRoundCards.add(0, currentCard);
    }
}
