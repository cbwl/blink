package fo.gt.blink.snap;

public class Player {
    private String name;
    private int score;

    public Player(int number) {
        this.name = "Player " + (number + 1);
        this.score = 0;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void won(int score) {
        this.score += score;
    }
}

