package fo.gt.blink.utils;

import java.util.Random;

public class ArrayOps {
    // https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle
    public static <T> void randomPermutationCopy(T[] source, T[] destination) {
        assert source.length == destination.length;
        System.arraycopy(source, 0, destination, 0, source.length);
        Random rand = new Random();
        for (int i = destination.length - 1; i > 0; --i) {
            int index = rand.nextInt(i + 1);
            T a = destination[index];
            destination[index] = destination[i];
            destination[i] = a;
        }
    }
}
