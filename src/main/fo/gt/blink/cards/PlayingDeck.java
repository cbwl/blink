package fo.gt.blink.cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PlayingDeck {
    private List<Card> cards;

    public PlayingDeck(int numDecks) {
        Card.Suit[] suits = Card.Suit.values();
        Card.Value[] values = Card.Value.values();

        int numCards = numDecks * suits.length * values.length;
        cards = new ArrayList<>(numCards);

        // This is slow, should factor out object creation of a standard deck. If still too slow could
        // replace Card/Suit/Value with base-13 numbers and then just use [0,52) but I Am Not A C Programmer
        for (int i = 0; i < numDecks; ++i) {
            for (int j = 0; j < suits.length; ++j) {
                for (int k = 0; k < values.length; ++k) {
                    Card c = new Card(suits[j], values[k]);
                    cards.add(c);
                }
            }
        }
    }

    public void shuffle() {
        synchronized (cards) {
            Collections.shuffle(cards);
        }
    }

    public Iterator<Card> iterator() {
        return cards.iterator();
    }
}
