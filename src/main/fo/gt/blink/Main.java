package fo.gt.blink;

import fo.gt.blink.matching.MatchingRule;
import fo.gt.blink.matching.SuitAndValueMatchingRule;
import fo.gt.blink.matching.SuitMatchingRule;
import fo.gt.blink.matching.ValueMatchingRule;
import fo.gt.blink.snap.Player;
import fo.gt.blink.snap.SnapConfig;
import fo.gt.blink.snap.SnapGame;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        new Main().run();
    }

    protected PrintStream out = System.out;
    protected InputStream in = System.in;

    public Main() {}

    protected void run() {
        out.println("Shall we play a game?");

        int numberOfDecks = askForNumberOfDecks();
        MatchingRule matchingRule = askForMatchingRule();

        SnapConfig snapConfig = new SnapConfig(numberOfDecks, matchingRule);
        SnapGame snapGame = new SnapGame(snapConfig, out);

        while (true) {
            try {
                boolean over = snapGame.step();
                if (over) break;
            } catch (Exception ex) {
                out.println("Quitting:");
                ex.printStackTrace(out);
                System.exit(1);
            }
        }

        printScores(snapGame.getPlayers());

        out.println("Thanks for playing!");
    }

    protected void printScores(Player[] players) {
        out.println("Scores:");
        Player winner = null;
        int maxScore = -1;
        for (int i = 0; i < players.length; ++i) {
            Player p = players[i];
            out.println(p.getName() + ": " + p.getScore());
            if (p.getScore() > maxScore) {
                winner = p;
                maxScore = p.getScore();
            } else if (p.getScore() == maxScore) {
                winner = null;
            }
        }
        if (winner != null) {
            out.println("WINNER: " + winner.getName());
        } else {
            out.println("DRAW.");
        }
    }

    protected int askForNumberOfDecks() {
        Scanner scanner = new Scanner(in);
        int numDecks = 0;
        out.print("Number of decks in the pile: ");
        while (true) {
            try {
                int input = Integer.parseUnsignedInt(scanner.next());
                numDecks = input;
                if (input > 0) break;
            } catch (NumberFormatException e) {}
            out.print("Enter an integer greater than 0: ");
        }
        return numDecks;
    }

    protected MatchingRule askForMatchingRule() {
        Scanner scanner = new Scanner(in);
        MatchingRule ret = null;
        out.print("Matching rule (suits/values/both): ");
        while (ret == null) {
            String input = scanner.next();
            switch(input) {
                case "suits":
                    ret = new SuitMatchingRule();
                    break;
                case "values":
                    ret = new ValueMatchingRule();
                    break;
                case "both":
                    ret = new SuitAndValueMatchingRule();
                    break;
                default:
                    out.print("Invalid input -- valid matching rules are suits, values, both: ");
                    break;
            }
        }
        return ret;
    }
}
