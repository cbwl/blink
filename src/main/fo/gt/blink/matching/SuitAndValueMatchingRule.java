package fo.gt.blink.matching;

import fo.gt.blink.cards.Card;

public class SuitAndValueMatchingRule implements MatchingRule {
    @Override
    public boolean matches(Card a, Card b) {
        return a.getSuit().equals(b.getSuit()) && a.getValue().equals(b.getValue());
    }
}
