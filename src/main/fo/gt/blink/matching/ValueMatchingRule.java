package fo.gt.blink.matching;

import fo.gt.blink.cards.Card;

public class ValueMatchingRule implements MatchingRule {
    @Override
    public boolean matches(Card a, Card b) {
        return a.getValue().equals(b.getValue());
    }
}
