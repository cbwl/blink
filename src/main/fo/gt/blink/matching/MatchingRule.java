package fo.gt.blink.matching;

import fo.gt.blink.cards.Card;

public interface MatchingRule {
    boolean matches(Card a, Card b);
}
